<?php

$arResult = array();
switch ($par1) {
    case 'news':
        $dataPage = $this->db->getPage('rnbw_news', false, "date desc", 1000);
        foreach ($dataPage['data'] as $d) {
            if ($d['prevu']) {
                $img = getImg($d['prevu']);
                $d['prevu'] = '/' . $img[0]['prevu'];
            }
            $arResult[] = array(
                'header' => $d['name'],
                'image' => $d['prevu'],
                'text' => $d['data']
            );
        }
        break;

    case 'form':
//        $_REQUEST = array(
//            'name'=>'name',
//            'phone'=>'+3 (523) 5235235',
//            'type'=>'3',
//        );
        switch($_REQUEST['type']){
            case 2 : $tt = 'Заказ обратного звонка'; break;
            case 3 : $tt = 'Заявка на акцию'; break;
            case 4 : $tt = 'Заказ карты «'.$_REQUEST['card']."»"; break;
            case 5 : $tt = 'Заказ карты + подарок'; break;
            default: exit;
        }
        $page_body = "<h1>$tt</h1>";
        if(isset($_REQUEST['name'])) $page_body .= "Имя: $_REQUEST[name]<br>";
        if(isset($_REQUEST['phone'])) $page_body .= "Телефон: $_REQUEST[phone]";
        if(isset($_REQUEST['zemail'])) $page_body .= "E-mail: $_REQUEST[zemail]";
        if($_REQUEST['type']==4) $page_body.="<br>Карта: ".$_REQUEST['card'];

        $mailOpt=array(
            "to" => $config['email'],
            "fromMail" => "robot@".$_SERVER['SERVER_NAME'],
            "from" => $_SERVER['SERVER_NAME'],
            "title" => $tt." на ".$_SERVER['SERVER_NAME'],
            "body" => $page_body,
        );
        print_r($mailOpt);
        exit;
        myMail($mailOpt);
        $arResult['status'] = 'ok';
        break;

    case 'schedule':
        include ('tmpl/schedule/base.php');
        foreach ($dataPage as $d) {
            $arItem = array(
                'date' => $daysN[$d['day']['dayname'] . ''] . ", " . $this->db->date_convert($d['day']['date'], 'getM&D')
            );
            foreach ($d['shed'] as $s) {
                $serv = $servicesName[$s['terms']];
                $sRub = $servRubr[$serv['parent']];
                $class = $colors[$gymsName[$s['gym']]['color']];

                if ($this->user->user) {
                    if (in_array($s['terms'], $userServs)) {
                        $ev = true;
                    } else {
                        $ev = false;
                    }
                } else {
                    $ev = false;
                }

                $arItem['items'][] = array(
                    'name' => $serv['name'],
                    'description' => $serv['data'],
                    'type' => '',
                    'duration' => $s['longs'] ? "$s[longs] мин" : '',
                    'teacher' => getName($teamName[$s['coach']]['name']),
                    'place' => $gymsName[$s['gym']]['name'],
                    'selected' => $ev,
                    'time' => substr($s['date'], 0, -3),
                );
            }
            $arResult[] = $arItem;
        }
//        print_r($arResult);
        break;
    default:
        break;
}
header('Content-Type: application/json');
echo json_encode($arResult);
exit;
