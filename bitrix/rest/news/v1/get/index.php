<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$arResult = array();
if (CModule::IncludeModule("iblock")) {
    $obCache = new \CPHPCache;
    $life_time = 3600;
    $cache_id = 'rest:news:v1';
    if ($obCache->InitCache($life_time, $cache_id, "/")) {
        $arResult = $obCache->GetVars();
    } elseif ($obCache->StartDataCache()) {
        $arSort = array(
            'SORT' => 'ASC',
            'ID' => 'ASC'
        );
        $arFilter = array(
            'IBLOCK_ID' => 9,
            'IBLOCK_ACTIVE' => 'Y',
            'ACTIVE' => 'Y',
            'CHECK_PERMISSIONS' => 'Y',
            'ACTIVE_DATE' => 'Y',
        );
        $returnFields = array(
            "ID", 'NAME', 'DETAIL_TEXT', 'PREVIEW_PICTURE'
        );

        $res = CIBlockElement::GetList($arSort, $arFilter, false, false, $returnFields);
        while ($arItem = $res->Fetch()) {
            if($arItem["PREVIEW_PICTURE"]) {
                $arItem["PREVIEW_PICTURE"] = cFile::GetPath($arItem["PREVIEW_PICTURE"]);
            }
//            $arItem["DISPLAY_DATE_CREATE"] = CIBlockFormatProperties::DateFormat('d.m.Y', MakeTimeStamp($arItem["DATE_CREATE"], CSite::GetDateFormat()));
            $arResult[] = array(
                'header'=>  mb_convert_encoding($arItem["NAME"], 'UTF-8', 'WINDOWS-1251'),
                'image'=>$arItem["PREVIEW_PICTURE"],
                'text'=>mb_convert_encoding($arItem["DETAIL_TEXT"], 'UTF-8', 'WINDOWS-1251')
            );
        }
        $obCache->EndDataCache($arResult);
    }
}
header('Content-Type: application/json');
echo json_encode($arResult);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
