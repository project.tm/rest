<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
chdir($_SERVER["DOCUMENT_ROOT"] . '/timetable/');
$command = $_REQUEST['TYPE'];

$arResult = array();
if (CModule::IncludeModule("iblock")) {
    $obCache = new \CPHPCache;
    $life_time = 3600;
    $cache_id = 'rest:timetable:v1:' . $command;
    if ($obCache->InitCache($life_time, $cache_id, "/")) {
        $arResult = $obCache->GetVars();
    } elseif ($obCache->StartDataCache()) {

        include ($_SERVER["DOCUMENT_ROOT"] . '/timetable/index.function.php');
        $arResult2 = array();
        foreach (array('lessons' => '��������� ���������', 'dc' => '������� � ������� ������') as $type => $typeName) {
            $_REQUEST['choice'] = $type;
            $_REQUEST['page'] = 0;
            include ($_SERVER["DOCUMENT_ROOT"] . '/timetable/index.update.function.php');
            foreach ($times as $key => $time) {
                for ($i = 3; $i < 9; $i++) {
                    foreach ($program as $key => $dann) {
                        $h = getdat2($pr, "$key", $i, $time, $days, $rasp, $_REQUEST['page'], $desc, $file);
                        if ($h) {
                            $h['type'] = $typeName;
                            $h['selected'] = false;
                            $h['time'] = $time;
                            $time2 = mktime(0, 0, 0, $days[$i]['m'], $days[$i]['d'], $days[$i]['y']);
                            $h['date'] = date('d.m.Y', $time2);
                            foreach ($h as $key2 => $value2) {
                                $h[$key2] = mb_convert_encoding($value2, 'UTF-8', 'WINDOWS-1251');
                            }
                            $arResult2[$time2][] = $h;
                        }
                    }
                }
            }
        }
        ksort($arResult2);
        foreach ($arResult2 as $time => $items) {
            $arResult[] = array(
                'date' => mb_convert_encoding($days_of_week[date('w', $time)] . ', ' . date('j', $time) . ' ' . $months_r[date('n', $time)], 'UTF-8', 'WINDOWS-1251'),
                'items' => $items
            );
        }
        $obCache->EndDataCache($arResult);
    }
}
header('Content-Type: application/json');
echo json_encode($arResult);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
